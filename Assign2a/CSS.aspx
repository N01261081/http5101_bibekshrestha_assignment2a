﻿<%@ Page Title="CSS" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CSS.aspx.cs" Inherits="Assign2a.WebForm2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="trickyConcept" runat="server">
    <h2>CSS float</h2>
    <p>The float property is used for positioning and layout on web pages.</p>
    <p>
        The float CSS property specifies that an element should be along the left or right side of its container, 
        allowing text and inline elements to wrap around it. The element is removed from the normal flow of the web page, 
        though still remaining a part of the flow (in contrast to absolute positioning).
    </p>
    <p>The float property is assigned a value, choosen from the list values below.</p>
    <ul>
        <li>none</li>
        <li>left</li>
        <li>right</li>
    </ul>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="myCode" runat="server">
    <p>The code below is a snippet from my assignment for CSS class. It just styles few elements in the content to look good.</p>
    <code>       .menu {
	        list-style: none;
	        padding-left: 0;
        }

        .hidden {
	        position: absolute;
	        left: -100000px;
        }

        .menu li {
	        display: inline-block;
	        margin-right: 1.5em;
        }

        #main-menu a{
	        font-weight: bold;
	        text-decoration: none;
        }

        #main-menu a, #main-menu a:visited {
	        color: #000;
        }

        .block-title {
	        font-weight: normal;
	        /*font-size: 1.5em;
	        line-height: 1;*/
	        text-transform: uppercase;
        }

        .button, .button:visited {
	        background-color: #bbb;
	        border: 1px solid #000;
	        padding: 5px 10px;
	        color: #000;
	        text-decoration: none;
	        /*border-radius: 5px;*/
	        /*border-radius: 5px 10px;*/
	        border-radius: 5px 20px 0;
        }</code>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="someCode" runat="server">
    <p>This code block below styles box with advanced effects. It was extracted from 
        <a href="https://developer.mozilla.org/en-US/docs/Learn/CSS/Styling_boxes/Advanced_box_effects" target="_blank">
            MDN | Advanced box effects
        </a>
    </p>
    <code>        p {
          margin: 0;
        }

        article {
          max-width: 500px;
          padding: 10px;
          background-color: red;
          background-image: linear-gradient(to bottom, rgba(0,0,0,0), rgba(0,0,0,0.25));
        }  

        .simple {
          box-shadow: 5px 5px 5px rgba(0,0,0,0.7);
        }</code>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="list" runat="server">
    <h3>Useful links</h3>
    <ul>
        <li><a href="https://www.w3schools.com/css/" target="_blank">W3SCHOOLS</a></li>
        <li><a href="https://css-tricks.com/" target="_blank">CSS Tricks</a></li>
        <li><a href="https://developer.mozilla.org/en-US/docs/Learn/CSS/Introduction_to_CSS" target="_blank">Mozilla Developer Network</a></li>
        <li><a href="https://www.codecademy.com/learn/learn-css" target="_blank">Codecademy</a></li>
    </ul>
</asp:Content>
